package com.example.carrental;

import com.example.carrental.entities.Car;
import com.example.carrental.entities.Customer;
import com.example.carrental.entities.Mechanik;
import com.example.carrental.repositories.AdressRepository;
import com.example.carrental.repositories.CarRepository;
import com.example.carrental.repositories.CustomerRepository;
import com.example.carrental.repositories.DrivingLicenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class CarrentalApplication implements CommandLineRunner {

    @Autowired
    AdressRepository adressRepository;

    @Autowired
    CarRepository carRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    DrivingLicenceRepository drivingLicenceRepository;

    public static void main(String[] args) {
        SpringApplication.run(CarrentalApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        //customerRepository.saveCustomerWithDrivingLicence();

		/*List<Customer> customers = new ArrayList<>();
		customers.add(customerRepository.getById(20003L));
		customers.add(customerRepository.getById(20004L));
		customerRepository.giveCustomersAnAdress(30004, customers);*/
        Car car = new Car("Trabant", "Old", 1969);
        Mechanik mechanik = new Mechanik("Florek", "Rasia");
        carRepository.insertCarAndMechanik(car, mechanik);
    }
}
