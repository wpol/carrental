package com.example.carrental.repositories;

import com.example.carrental.entities.Adress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class AdressRepository {
    @Autowired
    EntityManager em;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public Adress getById(long id) {
        return em.find(Adress.class, id);
    }

    public void deleteById(long id) {
        Adress adress = getById(id);
        em.remove(adress);
    }

    public Adress save(Adress adress) {
        if (adress.getId() == null) {
            em.persist(adress);
        } else {
            em.merge(adress);
        }
        return adress;
    }
}
