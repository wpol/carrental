package com.example.carrental.repositories;

import com.example.carrental.entities.Adress;
import com.example.carrental.entities.Customer;
import com.example.carrental.entities.DrivingLicence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class CustomerRepository {
    @Autowired
    EntityManager em;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public Customer getById(long id) {
        return em.find(Customer.class, id);
    }

    public void deleteById(long id) {
        Customer customer = getById(id);
        em.remove(customer);
    }

    public Customer save(Customer customer) {
        if (customer.getId() == null) {
            em.persist(customer);
        } else {
            em.merge(customer);
        }
        return customer;
    }

    public void saveCustomerWithDrivingLicence() {
        DrivingLicence drivingLicence = new DrivingLicence(555555, "Florek", "Bolo", "CC");
        em.persist(drivingLicence);
        Customer customer = new Customer("Florek", "Bolo");
        customer.setDrivinglicence(drivingLicence);
        em.persist(customer);
    }

    public void addNewCustomersForAdress(long adressId, List<Customer> customers) {
        Adress adress = em.find(Adress.class, adressId);
        for (Customer customer : customers) {
            adress.setCs(customer);
            customer.setAdress(adress);
            em.persist(customer);
        }
    }
    public void giveCustomersAnAdress(long adressId, List<Customer> customers) {
        Adress adress = em.find(Adress.class, adressId);
        for (Customer customer : customers) {
            adress.setCs(customer);
            customer.setAdress(adress);
            em.merge(customer);
        }
    }
}
