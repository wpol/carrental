package com.example.carrental.repositories;

import com.example.carrental.entities.Car;
import com.example.carrental.entities.Customer;
import com.example.carrental.entities.Mechanik;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class CarRepository {
    @Autowired
    EntityManager em;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public Car getById(long id) {
        return em.find(Car.class, id);
    }

    public void deleteById(long id) {
        Car car = getById(id);
        em.remove(car);
    }

    public Car save(Car car) {
        if (car.getId() == null) {
            em.persist(car);
        } else {
            em.merge(car);
        }
        return car;
    }

    public void addNewCarForCustomer(long customerId, List<Car> cars) {
        Customer customer = em.find(Customer.class, customerId);
        for (Car car : cars) {
            customer.setCars(car);
            car.setCustomer(customer);
            em.persist(car);
        }
    }
    public void giveCustomersAnAdress(long customerId, List<Car> cars) {
        Customer customer = em.find(Customer.class, customerId);
        for (Car car : cars) {
            customer.setCars(car);
            car.setCustomer(customer);
            em.merge(car);
        }
    }

    public void insertCarAndMechanik(Car car, Mechanik mechanik) {
        car.addMechanik(mechanik);
        mechanik.addCars(car);

        em.persist(car);
        em.persist(mechanik);
    }
}
