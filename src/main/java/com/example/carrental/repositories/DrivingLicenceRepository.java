package com.example.carrental.repositories;

import com.example.carrental.entities.DrivingLicence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class DrivingLicenceRepository {

    @Autowired
    EntityManager em;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public DrivingLicence getById(long id) {
        return em.find(DrivingLicence.class, id);
    }

    public void deleteById(long id) {
        DrivingLicence drivingLicence = getById(id);
        em.remove(drivingLicence);
    }

    public DrivingLicence save(DrivingLicence drivingLicence) {
        if (drivingLicence.getId() == null){
            em.persist(drivingLicence);
        }else {
            em.merge(drivingLicence);
        }
        return drivingLicence;
    }
}
