package com.example.carrental.repositories;

import com.example.carrental.entities.Mechanik;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class MechanikRepository {
    @Autowired
    EntityManager em;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public Mechanik getById(long id) {
        return em.find(Mechanik.class,id);
    }

    public void deleteById(long id) {
        Mechanik mechanik = getById(id);
        em.remove(mechanik);
    }

    public Mechanik save(Mechanik mechanik) {
        if (mechanik.getId() == null) {
            em.persist(mechanik);
        }else {
            em.merge(mechanik);
        }
        return mechanik;
    }
}
