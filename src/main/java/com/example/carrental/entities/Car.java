package com.example.carrental.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "car")
@NamedQueries(value = {
        @NamedQuery(name = "select_all", query = "select c from Car c"),
        @NamedQuery(name = "select_all_with_BMW", query = "SELECT c FROM Car c where c.make LIKE '%BMW%'")
})
public class Car {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "make", nullable = false)
    private String make;

    @Column(name = "model")
    private String model;

    @Column(name = "yearOfProduction")
    private int yearOfProduction;

    @ManyToOne
    private Customer customer;

    @ManyToMany
    @JoinTable(
            name = "CAR_MECHANIK",
            joinColumns = @JoinColumn(name = "CAR_ID"),
            inverseJoinColumns = @JoinColumn(name = "MECHANIK_ID")
    )
    private List<Mechanik> mechaniks = new ArrayList<>();

    public List<Mechanik> getMechaniks() {
        return mechaniks;
    }

    public void addMechanik(Mechanik mechanik) {
        this.mechaniks.add(mechanik);
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Car() {
    }

    public Car(String make, String model, int yearOfProduction) {
        this.make = make;
        this.model = model;
        this.yearOfProduction = yearOfProduction;
    }

    public Long getId() {
        return id;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public int getYearOfProduction() {
        return yearOfProduction;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setYearOfProduction(int yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", yearOfProduction=" + yearOfProduction +
                '}';
    }
}
