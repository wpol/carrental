package com.example.carrental.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "adress")
public class Adress {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "street")
    private String street;

    @Column(name = "number", nullable = false)
    private int number;

    @OneToMany(mappedBy = "adress")
    private List<Customer> cs = new ArrayList<>();

    public List<Customer> getCs() {
        return cs;
    }

    public void setCs(Customer cs) {
        this.cs.add(cs);
    }

    public Adress() {
    }

    public Adress(String city, String street, int number) {
        this.city = city;
        this.street = street;
        this.number = number;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Long getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return "Adress{" +
                "id=" + id +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", number=" + number +
                '}';
    }
}


