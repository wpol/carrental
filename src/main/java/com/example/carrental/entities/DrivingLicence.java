package com.example.carrental.entities;

import javax.persistence.*;

@Entity
@Table(name = "drivinglicence")
public class DrivingLicence {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "number", nullable = false)
    private int number;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "surname", nullable = false)
    private String surname;

    @Column(name = "category", nullable = false)
    private String category;

    @OneToOne(mappedBy = "drivinglicence")
    private Customer customer;

    public DrivingLicence() {
    }

    public DrivingLicence(int number, String name, String surname, String category) {
        this.number = number;
        this.name = name;
        this.surname = surname;
        this.category = category;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Long getId() {
        return id;
    }

    public int getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return "DrivingLicence{" +
                "id=" + id +
                ", number=" + number +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", category='" + category + '\'' +
                '}';
    }
}
