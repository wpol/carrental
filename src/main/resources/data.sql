--create table Car (ID INT PRIMARY KEY, MAKE VARCHAR(55), MODEL VARCHAR(50), yearOfProduction INT);
insert into Car(id, make, model, year_Of_Production) values(10001, 'BMW', '135', 2010);
insert into Car(id, make, model, year_Of_Production) values(10002, 'BMW', '1200', 2015);
insert into Car(id, make, model, year_Of_Production) values(10003, 'Audi', '7', 2017);
insert into Car(id, make, model, year_Of_Production) values(10004, 'Opel', 'Corsa', 2008);
insert into Car(id, make, model, year_Of_Production) values(10005, 'Mercedes', 'C', 2018);

insert into Adress(id, city, street, number) values(30001, 'Wroclaw', 'Ruska', 28);
insert into Adress(id, city, street, number) values(30002, 'Wroclaw', 'Nowa', 112);
insert into Adress(id, city, street, number) values(30003, 'Wroclaw', 'Gliniana', 13);
insert into Adress(id, city, street, number) values(30004, 'Wroclaw', 'Lotnicza', 175);
insert into Adress(id, city, street, number) values(30005, 'Wroclaw', 'Wielka', 9);
insert into DrivingLicence(id, number, name, surname, category) values(40001, 164686, 'Adam', 'Nowak', 'A2');
insert into DrivingLicence(id, number, name, surname, category) values(40002, 278749, 'Grzegorz', 'Kowalski', 'A2');
insert into DrivingLicence(id, number, name, surname, category) values(40003, 823148, 'Anna', 'Mickiewicz', 'A2');
insert into DrivingLicence(id, number, name, surname, category) values(40004, 952478, 'Robert', 'Florek', 'A2');
insert into DrivingLicence(id, number, name, surname, category) values(40005, 264165, 'Zenek', 'Brys', 'A2');
insert into Customer(id, name, surname, drivinglicence_id, adress_id) values(20001, 'Adam', 'Nowak', 40001, 30001);
insert into Customer(id, name, surname, drivinglicence_id, adress_id) values(20002, 'Grzegorz', 'Kowalski', 40002, 30002);
insert into Customer(id, name, surname, drivinglicence_id, adress_id) values(20003, 'Anna', 'Mickiewicz', 40003, 30003);
insert into Customer(id, name, surname, drivinglicence_id, adress_id) values(20004, 'Robert', 'Florek', 40004, 30003);
insert into Customer(id, name, surname, drivinglicence_id, adress_id) values(20005, 'Zenek', 'Brys', 40005, 30004);
insert into Mechanik(id, name, surname) values(50001, 'Adrian', 'Golas');
insert into Mechanik(id, name, surname) values(50002, 'Andrzej', 'Man');
insert into Mechanik(id, name, surname) values(50003, 'Gustaw', 'Drozda');
insert into Mechanik(id, name, surname) values(50004, 'Rycho', 'Daniec');
insert into Mechanik(id, name, surname) values(50005, 'Kamil', 'Kaczmarek');
Insert into car_mechanik(car_id, mechanik_id) VALUES (10001, 50001);
Insert into car_mechanik(car_id, mechanik_id) VALUES (10002, 50002);
Insert into car_mechanik(car_id, mechanik_id) VALUES (10003, 50003);
Insert into car_mechanik(car_id, mechanik_id) VALUES (10004, 50004);
Insert into car_mechanik(car_id, mechanik_id) VALUES (10005, 50005);
