package com.example.carrental;

import com.example.carrental.entities.DrivingLicence;
import com.example.carrental.repositories.DrivingLicenceRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarrentalApplication.class)
public class DrivingLicenceRepositoryTest {

    @Autowired
    EntityManager em;

    @Autowired
    DrivingLicenceRepository drivingLicenceRepository;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Test
    @DirtiesContext
    public void getByIdTest() {
        DrivingLicence drivingLicence = drivingLicenceRepository.getById(40001L);
        Assertions.assertThat(164686).isEqualTo(drivingLicence.getNumber());
    }

    @Test
    @DirtiesContext
    public void deleteByIdTest() {
        DrivingLicence drivingLicence = drivingLicenceRepository.getById(40001L);
        Assertions.assertThat(164686).isEqualTo(drivingLicence.getNumber());

        drivingLicenceRepository.deleteById(40001L);
        DrivingLicence drivingLicenceAfterDelete = drivingLicenceRepository.getById(40001L);
        Assertions.assertThat(drivingLicenceAfterDelete).isNull();
    }

    @Test
    @DirtiesContext
    public void insertTest() {
        DrivingLicence drivingLicence = drivingLicenceRepository.getById(1);
        Assertions.assertThat(drivingLicence).isNull();

        DrivingLicence drivingLicenceToInsert = new DrivingLicence(999999, "Trele", "Morele","AA");
        drivingLicenceRepository.save(drivingLicenceToInsert);

        DrivingLicence drivingLicenceAfterInsert = drivingLicenceRepository.getById(1);
        Assertions.assertThat(999999).isEqualTo(drivingLicenceAfterInsert.getNumber());
    }

    @Test
    @DirtiesContext
    public void updateTest() {
        DrivingLicence drivingLicence = drivingLicenceRepository.getById(40002L);
        Assertions.assertThat(278749).isEqualTo(drivingLicence.getNumber());

        drivingLicence.setNumber(111111);
        drivingLicenceRepository.save(drivingLicence);

        DrivingLicence drivingLicenceAfterUpdate = drivingLicenceRepository.getById(40002L);
        Assertions.assertThat(111111).isEqualTo(drivingLicenceAfterUpdate.getNumber());
    }
}
