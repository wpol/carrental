package com.example.carrental;

import com.example.carrental.entities.Customer;
import com.example.carrental.entities.DrivingLicence;
import com.example.carrental.repositories.CustomerRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarrentalApplication.class)
public class CustomerRepositoryTest {
    @Autowired
    EntityManager em;

    @Autowired
    CustomerRepository customerRepository;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Test
    @DirtiesContext
    public void getByIdTest() {
        Customer customer = customerRepository.getById(20001L);
        Assertions.assertThat("Nowak").isEqualTo(customer.getSurname());
    }

    @Test
    @DirtiesContext
    public void deleteByIdTest() {
        Customer customer = customerRepository.getById(20001L);
        Assertions.assertThat("Nowak").isEqualTo(customer.getSurname());

        customerRepository.deleteById(20001L);
        Customer customerAfterDelete = customerRepository.getById(20001L);
        Assertions.assertThat(customerAfterDelete).isNull();
    }

    @Test
    @DirtiesContext
    public void insertTest() {
        Customer customer = customerRepository.getById(1);
        Assertions.assertThat(customer).isNull();

        Customer newCustomer = new Customer("Ala", "Bobas");
        customerRepository.save(newCustomer);

        Customer customerAfterInsert = customerRepository.getById(1);
        Assertions.assertThat("Bobas").isEqualTo(customerAfterInsert.getSurname());
    }

    @Test
    @DirtiesContext
    public void updateTest() {
        Customer customer = customerRepository.getById(20002L);
        Assertions.assertThat("Kowalski").isEqualTo(customer.getSurname());

        customer.setSurname("Baba");
        customerRepository.save(customer);

        Customer customerAfterUpdate = customerRepository.getById(20002L);
        Assertions.assertThat("Baba").isEqualTo(customerAfterUpdate.getSurname());
    }

    @Test
    @Transactional
    @DirtiesContext
    public void findByIdWithDrivingLicenceTest() {
        Customer customer = em.find(Customer.class,20003L);
        logger.info("Customer -> {}", customer);
        logger.info("DrivingLicence -> {}", customer.getDrivinglicence());
    }

    @Test
    @Transactional
    @DirtiesContext
    public void findCustomerByIdViaDrivingLicenceTest() {
        DrivingLicence drivingLicence = em.find(DrivingLicence.class, 40005L);
        logger.info("DrivingLicence -> {}", drivingLicence);
        logger.info("Customer -> {}", drivingLicence.getCustomer());
    }
}
