package com.example.carrental;

import com.example.carrental.entities.Mechanik;
import com.example.carrental.repositories.MechanikRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarrentalApplication.class)
public class MechanikRepositoryTest {
    @Autowired
    EntityManager em;

    @Autowired
    MechanikRepository mechanikRepository;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Test
    @DirtiesContext
    public void getByIdTest() {
        Mechanik mechanik = mechanikRepository.getById(50001L);
        Assertions.assertThat("Golas").isEqualTo(mechanik.getSurname());
    }

    @Test
    @DirtiesContext
    public  void deleteByIdTest() {
        Mechanik mechanik = mechanikRepository.getById(50002L);
        Assertions.assertThat("Man").isEqualTo(mechanik.getSurname());

        mechanikRepository.deleteById(50002L);
        Mechanik mechanikAfterDelete = mechanikRepository.getById(50002L);
        Assertions.assertThat(mechanikAfterDelete).isNull();
    }

    @Test
    @DirtiesContext
    public void insertTest() {
        Mechanik mechanik = mechanikRepository.getById(1);
        Assertions.assertThat(mechanik).isNull();

        Mechanik mechanikToInsert = new Mechanik("Hehe","HOHO");
        mechanikRepository.save(mechanikToInsert);

        Mechanik mechanikAfterInsert = mechanikRepository.getById(1);
        Assertions.assertThat("HOHO").isEqualTo(mechanikAfterInsert.getSurname());
    }

    @Test
    @DirtiesContext
    public void updateTest() {
        Mechanik mechanik = mechanikRepository.getById(50003L);
        Assertions.assertThat("Drozda").isEqualTo(mechanik.getSurname());

        mechanik.setSurname("Fiufiu");
        mechanikRepository.save(mechanik);

        Mechanik mechanikAfterUpdate = mechanikRepository.getById(50003L);
        Assertions.assertThat("Fiufiu").isEqualTo(mechanikAfterUpdate.getSurname());
    }
}
