package com.example.carrental;

import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarrentalApplication.class)
public class JPQLTest {
    @Autowired
    EntityManager em;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public void getAllCarsByNamedQuerry() {
        List cars = em.createNamedQuery("select_all").getResultList();
        logger.info("Cars -> {}", cars);
    }
}
