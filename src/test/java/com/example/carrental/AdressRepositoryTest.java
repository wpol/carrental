package com.example.carrental;

import com.example.carrental.entities.Adress;
import com.example.carrental.repositories.AdressRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarrentalApplication.class)
public class AdressRepositoryTest {

    @Autowired
    EntityManager em;

    @Autowired
    AdressRepository adressRepository;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Test
    @DirtiesContext
    public void getByIdTest() {
        Adress adress = adressRepository.getById(30001L);
        Assertions.assertThat("Ruska").isEqualTo(adress.getStreet());
    }

    @Test
    @DirtiesContext
    public void deleteByIdTest() {
        Adress adress = adressRepository.getById(30001L);
        Assertions.assertThat("Ruska").isEqualTo(adress.getStreet());

        adressRepository.deleteById(30001L);
        Adress adressAfterDelete = adressRepository.getById(30001L);
        Assertions.assertThat(adressAfterDelete).isNull();
    }

    @Test
    @DirtiesContext
    public void insertTest() {
        Adress adress = adressRepository.getById(1);
        Assertions.assertThat(adress).isNull();

        Adress newAdress = new Adress("Wroclaw", "Dluga", 56);
        adressRepository.save(newAdress);

        Adress adressAfterInsert = adressRepository.getById(1);
        Assertions.assertThat("Dluga").isEqualTo(adressAfterInsert.getStreet());
    }

    @Test
    @DirtiesContext
    public void updateTest() {
        Adress adress = adressRepository.getById(30002L);
        Assertions.assertThat("Nowa").isEqualTo(adress.getStreet());

        adress.setStreet("Stara");
        adressRepository.save(adress);

        Adress adressAfterUpdate = adressRepository.getById(30002L);
        Assertions.assertThat("Stara").isEqualTo(adressAfterUpdate.getStreet());
    }

    @Test
    @Transactional
    @DirtiesContext
    public void getCustomersForAdress() {
        Adress adress = adressRepository.getById(30003L);
        logger.info("{}", adress.getCs());
    }
}
