package com.example.carrental;

import com.example.carrental.entities.Car;
import com.example.carrental.repositories.CarRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CarrentalApplication.class)
public class CarRepositoryTest {
    @Autowired
    EntityManager em;

    @Autowired
    CarRepository carRepository;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Test
    @DirtiesContext
    public void getByIdTest() {
        Car car = carRepository.getById(10001L);
        Assertions.assertThat("135").isEqualTo(car.getModel());
    }

    @Test
    @DirtiesContext
    public void deleteByIdTest() {
        Car car = carRepository.getById(10001L);
        Assertions.assertThat("135").isEqualTo(car.getModel());

        carRepository.deleteById(10001L);
        Car carAfterDelete = carRepository.getById(10001L);
        Assertions.assertThat(carAfterDelete).isNull();
    }

    @Test
    @DirtiesContext
    public void insertTest() {
        Car car = carRepository.getById(1);
        Assertions.assertThat(car).isNull();

        Car newCar = new Car("Fiat", "125", 1999);
        carRepository.save(newCar);

        Car carAfterInsert = carRepository.getById(1);
        Assertions.assertThat("125").isEqualTo(carAfterInsert.getModel());
    }

    @Test
    @DirtiesContext
    public void updateTest() {
        Car car = carRepository.getById(10002L);
        Assertions.assertThat("1200").isEqualTo(car.getModel());

        car.setModel("1111");
        carRepository.save(car);

        Car carAfterUpdate = carRepository.getById(10002L);
        Assertions.assertThat("1111").isEqualTo(carAfterUpdate.getModel());
    }

    @Test
    @Transactional
    @DirtiesContext
    public void getCarAndAllMechaniks() {
        Car car = em.find(Car.class, 10003L);
        logger.info("Car -> {}", car);
        logger.info("Mechaniks -> {}", car.getMechaniks());
    }
}

